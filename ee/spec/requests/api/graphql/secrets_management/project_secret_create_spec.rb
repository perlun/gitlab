# frozen_string_literal: true

require 'spec_helper'

RSpec.describe 'Create project secret', :gitlab_secrets_manager, feature_category: :secrets_management do
  include GraphqlHelpers

  let_it_be_with_reload(:project) { create(:project) }
  let_it_be(:current_user) { create(:user) }
  let_it_be(:mutation_name) { :project_secret_create }

  let(:secrets_manager) { create(:project_secrets_manager, project: project) }

  let(:params) do
    {
      project_path: project.full_path,
      name: 'TEST_SECRET',
      description: 'test description',
      value: 'the-secret-value',
      branch: 'main',
      environment: 'prod'
    }
  end

  let(:mutation) { graphql_mutation(mutation_name, params) }
  let(:mutation_response) { graphql_mutation_response(mutation_name) }

  subject(:post_mutation) { post_graphql_mutation(mutation, current_user: current_user) }

  before do
    stub_last_activity_update
    provision_project_secrets_manager(secrets_manager, current_user)
  end

  context 'when current user is not part of the project' do
    it_behaves_like 'a mutation on an unauthorized resource'
  end

  context 'when current user is not the project owner' do
    before_all do
      project.add_maintainer(current_user)
    end

    it_behaves_like 'a mutation on an unauthorized resource'
  end

  context 'when current user is the project owner' do
    before_all do
      project.add_owner(current_user)
    end

    it_behaves_like 'internal event tracking' do
      let(:event) { 'create_ci_secret' }
      let(:user) { current_user }
      let(:namespace) { project.namespace }
      let(:additional_properties) { { label: 'graphql' } }
      let(:category) { 'Mutations::SecretsManagement::ProjectSecretCreate' }
    end

    it 'creates the project secret', :aggregate_failures do
      post_mutation

      expect(response).to have_gitlab_http_status(:success)
      expect(mutation_response['errors']).to be_empty

      expect(graphql_data_at(mutation_name, :project_secret))
        .to match(a_graphql_entity_for(
          project: a_graphql_entity_for(project),
          name: params[:name],
          description: params[:description],
          branch: params[:branch],
          environment: params[:environment]
        ))
    end

    context 'and service results to a failure' do
      before do
        allow_next_instance_of(SecretsManagement::CreateProjectSecretService) do |service|
          allow(service).to receive(:execute).and_return(ServiceResponse.error(message: 'some error'))
        end
      end

      it 'returns the service error' do
        expect_next_instance_of(SecretsManagement::CreateProjectSecretService) do |service|
          project_secret = SecretsManagement::ProjectSecret.new
          project_secret.errors.add(:base, 'some error')

          result = ServiceResponse.error(message: 'some error', payload: { project_secret: project_secret })
          expect(service).to receive(:execute).and_return(result)
        end

        post_mutation

        expect(mutation_response['errors']).to include('some error')
      end

      it_behaves_like 'internal event not tracked'
    end

    context 'and secrets_manager feature flag is disabled' do
      it 'returns an error' do
        stub_feature_flags(secrets_manager: false)

        post_mutation

        expect_graphql_errors_to_include("`secrets_manager` feature flag is disabled.")
      end
    end
  end
end
